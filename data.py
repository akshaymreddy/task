import pandas as pd

purchase_data = pd.read_csv('purchase_data.csv',sep=',',names =["order_id", "isbn", "publisher", "school","price","duration","order_datetime"]).apply(lambda x: x.astype(str).str.lower())
purchase_buckets = pd.read_csv('purchase_buckets.csv',sep=',',names = ["publisher", "price", "duration"]).apply(lambda x: x.astype(str).str.lower())

# purchase_data = pd.read_csv('sample_data.csv',sep=',',names =["order_id", "isbn", "publisher", "school","price","duration","order_datetime"]).apply(lambda x: x.astype(str).str.lower())
# purchase_buckets = pd.read_csv('sample_buckets.csv',sep=',',names = ["publisher", "price", "duration"]).apply(lambda x: x.astype(str).str.lower())
